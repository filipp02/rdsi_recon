# Radial Diffusion Spectrum Imaging (RDSI): Demo DWI data


Data simulated with [Phantomas](http://www.emmanuelcaruyer.com/phantomas.php) to use as an example for RDSI reconstruction.

## Simulation details

* Install Phantomas according to the instructions on [http://www.emmanuelcaruyer.com/phantomas.php](http://www.emmanuelcaruyer.com/phantomas.php) and [https://github.com/ecaruyer/phantomas](https://github.com/ecaruyer/phantomas).
* The configuration used was: isbi_challange_2013.txt.

Create structural MRI

	phantomas_struct -o demodata --snr 50 --res 2.0 isbi_challenge_2013.txt
	
Create DWI MRI

	phantomas_dwis -b RDSI_withb0_252dirs.bval -r RDSI_withb0_252dirs.bvec --output_dir demodata --res 2.0 --snr 50.0 isbi_challenge_2013.txt
